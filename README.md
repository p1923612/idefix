# IDEFIX

## Membres

Projet réalisé par Yilei Pan, Anthony Scriven, Malo Borowy--Vanackere, Aristide Dullin, Victor Combat et Mickael Chemoul.

## État du projet

| Tâches                                    | Status                    |
| ----------------------------------------- | ------------------------- |
| Montage lego (avant le confinement)       | <ul><li>- [x] </li></ul>  |
| Module en python pour la gestion de D-BUS | <ul><li>- [x] </li></ul>  |
| Suivi de la ligne                         | <ul><li>- [x] </li></ul>  |
| Lecture du code                           | <ul><li>- [x] </li></ul> |
| Gestion des intersections (sans stop) | <ul><li>- [x] </li></ul> |
| Stop à l'intersection si nécessaire | <ul><li>- [x] </li></ul> |
| Stop quand il y a des obstacles (Bonus)   | <ul><li>- [x] </li></ul> |
| Demi tour lors d'une voie sans issue      | <ul><li>- [x] </li></ul> |
| Carte personnalisée (Bonus)               | <ul><li>- [x] </li></ul>  |


## Fonctionnement

Le fonctionnement global se résume en 3 étapes. 

- **Étape 1: Initialiser le terrain et le robot**
  
  - Ouvrir un fichier `.playground` avec Aseba playground. Pour changer le terrain, il suffit de modifier le champ `<world/>` et mettre l'image voulue. Notre fichier se nomme `mapTeamPi.playground` et utilise le circuit
`lilo_et_stitch.png` dans le dossier `/circuit`. 
  
- **Étape 2: Se connecter au robot via dbus**:
  
  - Dans un terminal, exécuter la commande: `asebamedulla "tcp:localhost;33333" -p 33334` et conserver son exécution en tache de fond
  
- **Étape 3: Exécuter le script python pour contrôler le robot**
  - Commande: `python3 control.py`
	
	Le fichier `control.py` se trouve dans `/code`
    Il est également possible de charger un script aseba avant l'exécution du code.

  - Commande: `python3 control.py script.aesl`
