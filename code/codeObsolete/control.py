# Dbus will allow python to communicate with the thymio through asebamedulla
import dbus
import dbus.mainloop.glib
# Gojbect is used to run and handle the loop
from gi.repository import GObject as gobject
from optparse import OptionParser
# Sys will be used to receive key presses
import sys
import time
# Import the mini aseba library (aseba.py)
from aseba import *

# Cette méthode sera appelée toutes les 0.1 sec
def main(aseba):
    # Étape1: récupérer les données des deux capteurs au sol.
    # Dans la simulation on s'intéresse à connaitre prox.ground.reflected

    groundReflected = aseba.get("thymio-II", "prox.ground.reflected")
    intensity = groundReflected[1] - 525
    if abs(intensity) < 170:
        aseba.set("thymio-II", "motor.left.target", [115 + intensity//8])
        aseba.set("thymio-II", "motor.right.target", [115 + intensity//8])
    else:
        aseba.set("thymio-II", "motor.left.target", [intensity//4])
        aseba.set("thymio-II", "motor.right.target", [-intensity//4])
    return True

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-s", "--system", action="store_true", dest="system", default=False,help="use the system bus instead of the session bus")
 
    (options, args) = parser.parse_args()
    
    aseba = Aseba(options.system)

    gobject.timeout_add (100, main, aseba)
    aseba.run()