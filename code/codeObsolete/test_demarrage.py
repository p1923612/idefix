# Dbus will allow python to communicate with the thymio through asebamedulla
import dbus
import dbus.mainloop.glib
# Gojbect is used to run and handle the loop
from gi.repository import GObject as gobject
from optparse import OptionParser
# Sys will be used to receive key presses
import sys
import time
# Import the mini aseba library (aseba.py)
from aseba import *

def Mystuff(aseba):
    groundValue = aseba.get("thymio-II", "prox.ground.reflected") # Intensité lumineuse pour les deux capteurs
    print(groundValue)

    if groundValue[1] < 250:
        #aseba.set("thymio-II", "motor.left.target", [-100])
        aseba.set("thymio-II", "motor.left.target", [100 - (groundValue[1] - 250)])
        aseba.set("thymio-II", "motor.right.target", [100])
    elif groundValue[1] > 250 and groundValue[1] < 750:
        aseba.set("thymio-II", "motor.left.target", [100])
        aseba.set("thymio-II", "motor.right.target", [100])
    else:
        aseba.set("thymio-II", "motor.left.target", [100])
        #aseba.set("thymio-II", "motor.right.target", [-100])
        aseba.set("thymio-II", "motor.right.target", [100 - (groundValue[1] - 750)])

    return True
 
if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-s", "--system", action="store_true", dest="system", default=False,help="use the system bus instead of the session bus")
 
    (options, args) = parser.parse_args()
    
    aseba = Aseba(options.system)

    gobject.timeout_add (100, Mystuff, aseba)
    aseba.run()