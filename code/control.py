# coding: utf8

from thymio import *
import sys
import os
import random as r
from enum import Enum
from optparse import OptionParser

# ------ Constants ------------
class Color(Enum):
    BLACK = 0
    DARK_GRAY = 1
    LIGHT_GRAY = 2
    WHITE = 3


class Case(Enum):
    SIMPLE = 0
    TRI = 1
    QUAD = 2


BW_LIMIT = 300
SIGMA = 95
INTERVAL = 5

### Tri-codes ###
LF1 = [Color.LIGHT_GRAY, Color.WHITE, Color.BLACK, Color.LIGHT_GRAY]
LF2 = [Color.LIGHT_GRAY, Color.DARK_GRAY, Color.WHITE, Color.DARK_GRAY]
RF1 = [Color.LIGHT_GRAY, Color.WHITE, Color.BLACK, Color.WHITE]
RF2 = [Color.LIGHT_GRAY, Color.LIGHT_GRAY, Color.WHITE, Color.DARK_GRAY]
LFS = [Color.LIGHT_GRAY, Color.WHITE, Color.DARK_GRAY, Color.BLACK]
RFS = [Color.LIGHT_GRAY, Color.WHITE, Color.LIGHT_GRAY, Color.BLACK]
LR1 = [Color.LIGHT_GRAY, Color.DARK_GRAY, Color.WHITE, Color.WHITE]
LR2 = [Color.LIGHT_GRAY, Color.LIGHT_GRAY, Color.WHITE, Color.LIGHT_GRAY]
LRS = [Color.LIGHT_GRAY, Color.BLACK, Color.WHITE, Color.BLACK]

### Quad-codes ###
Q1 = [Color.BLACK, Color.LIGHT_GRAY, Color.WHITE, Color.WHITE]
Q2 = [Color.BLACK, Color.DARK_GRAY, Color.WHITE, Color.WHITE]
QS1 = [Color.BLACK, Color.WHITE, Color.DARK_GRAY, Color.WHITE]
QS2 = [Color.BLACK, Color.WHITE, Color.LIGHT_GRAY, Color.WHITE]
QS3 = [Color.BLACK, Color.BLACK, Color.WHITE, Color.WHITE]
Q3 = [Color.BLACK, Color.WHITE, Color.BLACK, Color.WHITE]
QS4 = [Color.BLACK, Color.WHITE, Color.WHITE, Color.WHITE]

# Interval for each color
white_interval = [900, 1000]
light_gray_interval = [720, 900]
dark_gray_interval = [280, 720]
black_interval = [0, 280]

# ------Global variables ------
code_buffer = [0, 0, 0, 0]
speed = 115
deviation = 0
mini = 1024
maxi = 0
mean = 512
variation = 512
ndev = 0
ireg = 0
pcoeff = 30
icoeff = 30
running = 0
counter = 0
state = 0
left = 0
right = 0
out_of_track = 0
out = False
in_intersection = 0
direction = 1
trigger = False
wait = False # Les stops à l'intersection
stop = False # Pour la detection de l'obstacle
situation = Case.SIMPLE

# ------Main procedure --------
def main(thymio):
    ground_reflected = thymio.get("prox.ground.delta")
    left_ground_sensor = ground_reflected[0]
    right_ground_sensor = ground_reflected[1]
    detect_obstacle(thymio)
    if not stop:
        if situation == Case.SIMPLE:
            follow_line(thymio, right_ground_sensor)
            read_code(thymio, left_ground_sensor)
        elif situation == Case.TRI:
            threeway_intersection(thymio, ground_reflected)
        elif situation == Case.QUAD:
            fourway_intersection(thymio, ground_reflected)
        else:
            raise Exception("Case doesn't exist")
    else:
        turn_off(thymio)

    return True

def detect_obstacle(thymio):
    global stop
    limitFront=3000
    limitSide=5000
    # 0 avant gauche, 2 milieu, 4 avant droite
    horizontal = thymio.get("prox.horizontal")
    front = horizontal[2]
    left = horizontal[1]
    right = horizontal[3]
    if front > limitFront or left>limitSide or right>limitSide:
        stop = True
    else:
        stop=False


def follow_line(thymio, right_ground_sensor):
    '''
    Cette fonction permet de suivre une ligne simple
    Fonction écrite sur le modèle de edge-follower.aesl
    Honnêtement, nous ne comprenons pas entièrement les calculs effectués (ndev,preg,ireg...)
    '''
    global ndev, variation, ireg, left, right, state, out_of_track, out
    if not out:
        calculate_values(thymio, right_ground_sensor)
    ndev = (SIGMA * (right_ground_sensor - mean)) / variation   # Permet de faire un suivi de ligne précis.
    if abs(ndev) <= SIGMA and out_of_track < 5:  # Cas où Thymio est sur une ligne droite
        out_of_track = 0
        out = False
        preg = (pcoeff * ndev) / 100
        ireg += (icoeff * preg) / 100
        left = speed + (preg + ireg)
        right = speed - (preg + ireg)
        thymio.set("motor.left.target", [left])
        thymio.set("motor.right.target", [right])
    else:
        # nous avons perdu la piste
        out_of_track += 1
        ireg = 0
        thymio.set("motor.left.target", [1 * ndev / 2])  # et on tourne à la place afin de retrouver la linge
        thymio.set("motor.right.target", [-1 * ndev / 2])
        # Si Thymio à perdu la piste pendant un certain temps, il essaye de faire demi-tour
        if out_of_track >= 5:
            out = True
            state = 0
            thymio.set("motor.left.target", [0])
            thymio.set("motor.right.target", [500])
            # Si il retrouve une ligne noire, il se remet à la suivre
            if right_ground_sensor < dark_gray_interval[1]:
                out_of_track = 0
                thymio.set("motor.left.target", [left])
                thymio.set("motor.right.target", [right])


def read_code(thymio, left_ground_sensor):
    global code_buffer, state, counter, situation, trigger
    # Lorsque le capteur gauche détecte une couleur très sombre, il se met dans l'état de lecture
    if state == 0 and left_ground_sensor < BW_LIMIT:
        state = 1
        # call leds.top(0,0,0)
        print("Sync code detected, start lecture:")
        counter = 0
    # print(str(left_ground_sensor) + ", 0")
    # state == 1 implique que Thymio se trouve dans l'état de lecture du code
    if state == 1:
        # INTERVAL est défini à 5 par défaut. Nous attendons INTERVAL pas de temps afin que Thymio ait dépassé la case de synchronisation
        # Ici, Thymio doit lire la première case du code
        if counter == INTERVAL:
            code_buffer[0] = transform_prox_value(left_ground_sensor)
            # print(str(left_ground_sensor) + ", 1")
        # On attend 2*INTERVAL+1 afin d'être sûr d'être dans la case suivante
        elif counter == (2 * INTERVAL):
            code_buffer[1] = transform_prox_value(left_ground_sensor)
            # print(str(left_ground_sensor) + ", 1")
        # Troisième case
        elif counter == (3 * INTERVAL):
            code_buffer[2] = transform_prox_value(left_ground_sensor)
            # print(str(left_ground_sensor) + ", 1")
        # Quatrième et dernière case
        elif counter == (4 * INTERVAL):
            code_buffer[3] = transform_prox_value(left_ground_sensor)
            # print(str(left_ground_sensor) + ", 1")
            print(code_buffer)
            # Changer la situation selon le code à la fin de lecture
            if code_buffer[0] == Color.LIGHT_GRAY:
                trigger = True
                situation = Case.TRI
                # Changer tout de suite l'état de lecture. 
                # Car Thymio ne va pas lire le code hors cas simple
                state = 0
            elif code_buffer[0] == Color.BLACK:
                trigger = True
                situation = Case.QUAD
                # Changer tout de suite l'état de lecture. 
                # Car Thymio ne va pas lire le code hors cas simple
                state = 0
            else:
                # Ne pas changer tout de suite l'état de lecture. 
                # Il risque d'avoir plusieurs codes enchaînés
                situation = Case.SIMPLE
        # Attente d'encore un INTERVAL afin de se préparer au code suivant
        elif counter == 5 * INTERVAL:
            state = 0
        counter += 1


def threeway_intersection(thymio, ground_reflected):
    global situation, in_intersection, state, trigger, direction, wait
    # La traversée de l'intersection prend 160 tours environ
    if in_intersection < 160:
        state = 0
        # Booléen utilisé pour ne tirer la direction qu'une seule fois
        if trigger:
            # On tire un nombre aléatoire qui déterminera la direction de Thymio
            direction = r.randint(0, 1)
            trigger = False
        # Cas où les directions possibles sont en face et à gauche    
        if code_buffer == LF1 or code_buffer == LF2 or code_buffer == LFS:
            #Si le stop est pour Thymio
            if code_buffer == LFS:
                # Au bout de 60 tours, il s'arrête
                if (int(in_intersection) == 60) and not wait:
                    wait = True
                    print("Thymio cède la priorité.")
                #Et redémarre vers 112 tours (mesures empiriques et en accord avec l'incrément du compteur)
                elif (int(in_intersection) == 112) and wait:
                    wait = False
                    print("Thymio redémarre.")
                    # On soustrait au compteur le temps passé à attendre afin de reprendre le déroulement de l'intersection correctement
                    in_intersection -= 51
                if wait:
                    turn_off(thymio)
            if direction:
                # l'incrémentation du compteur est différente selon l'intersection à cause de la longueur des virages
                in_intersection += 1.5
                if not wait:
                    go_front(thymio)
            else:
                # Tourner à gauche prend du temps (car le virage est plus large) donc le compteur ne s'incrémente que de 1
                in_intersection += 1
                if not wait:
                    turn_left(thymio) 
        # Cas où les directions possibles sont en face et à droite   
        elif code_buffer == RF1 or code_buffer == RF2 or code_buffer == RFS:
            if code_buffer == RFS:
                if (int(in_intersection) == 60) and not wait:
                    wait = True
                    print("Thymio cède la priorité.")
                elif (int(in_intersection) == 112) and wait:
                    wait = False
                    print("Thymio redémarre.")
                    in_intersection -= 51
                if wait:
                    turn_off(thymio)
            if direction:
                in_intersection += 1.5
                if not wait:
                    go_front(thymio)
            else:
                #Tourner à droite est plus rapide donc le compteur s'incrémente plus vite
                in_intersection += 2
                if not wait:
                    turn_right(thymio)
        # Cas où les directions possibles sont à droite et à gauche    
        elif code_buffer == LR1 or code_buffer == LR2 or code_buffer == LRS:
            if code_buffer == LRS:
                if (int(in_intersection) == 60) and not wait:
                    wait = True
                    print("Thymio cède la priorité.")
                elif (int(in_intersection) == 112) and wait:
                    wait = False
                    print("Thymio redémarre.")
                    in_intersection -= 51
                if wait:
                    turn_off(thymio)
            if direction:
                in_intersection += 1
                if not wait:
                    turn_left(thymio)
            else:
                in_intersection += 2
                if not wait:
                    turn_right(thymio)
    else:
        # On remet le compteur à 0
        in_intersection = 0
        # On se remet dans un cas simple afin de pouvoir lire le code suivant
        situation = Case.SIMPLE
        # à la fin de la traversée, on réactive le suivi de ligne
        follow_line(thymio, ground_reflected[1])


def fourway_intersection(thymio, ground_reflected):
    global situation, in_intersection, state, trigger, direction, wait
    if in_intersection < 160:
        state = 0
        if trigger:
            # Le fonctionnement est similaire à celui des intersections à 3 voies avec une différence sur le nombre de directions possibles.
            direction = r.randint(0, 2)
            trigger = False
        if code_buffer == QS1 or code_buffer == QS2 or code_buffer == QS3 or code_buffer == QS4:
            if (int(in_intersection) == 60) and not wait:
                wait = True
                print("Thymio cède la priorité.")
            elif (int(in_intersection) == 112) and wait:
                wait = False
                print("Thymio redémarre.")
                in_intersection -= 51
            if wait:
                turn_off(thymio)
        if direction == 0:
            in_intersection += 1
            if not wait:
                turn_left(thymio) 
        elif direction == 1:
            in_intersection += 1.5
            if not wait:
                go_front(thymio)
        else:
            in_intersection += 2
            if not wait:
                turn_right(thymio)
    else:
        in_intersection = 0
        situation = Case.SIMPLE
        follow_line(thymio, ground_reflected[1])

# Fonctions permettant de suivre la direction choisie par Thymio pendant les intersections :
   
def go_front(thymio):
    thymio.set("motor.left.target", [speed])
    thymio.set("motor.right.target", [speed])
    print("Thymio va en face.")

def turn_left(thymio):
    thymio.set("motor.left.target",[68])
    thymio.set("motor.right.target",[85])
    print("Thymio tourne à gauche.")

def turn_right(thymio):
    thymio.set("motor.left.target", [142])
    thymio.set("motor.right.target", [100])
    print("Thymio tourne à droite.")

# Coupe les moteurs de Thymio
def turn_off(thymio):
    thymio.set("motor.left.target",[0])
    thymio.set("motor.right.target",[0])

# ---------------- Fonctions des calculs ---------------------------------
def calculate_values(thymio, right_ground_sensor):
    global maxi, mini, variation, mean
    maxi = max(maxi, right_ground_sensor)
    mini = min(mini, right_ground_sensor)
    if maxi - mini > 400:
        variation = (45 * (maxi - mini)) / 100
        mean = (mini + maxi) / 2
        # print(mean)


def transform_prox_value(value):
    if value > white_interval[0]:
        return Color.WHITE
    elif value > light_gray_interval[0] and value <= light_gray_interval[1]:
        return Color.LIGHT_GRAY
    elif value > dark_gray_interval[0] and value <= dark_gray_interval[1]:
        return Color.DARK_GRAY
    elif value <= black_interval[1]:
        return Color.BLACK


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-s", "--system", action="store_true", dest="system", default=False,help="use the system bus instead of the session bus")
 
    (options, args) = parser.parse_args()
    # check command-line arguments
    if len(sys.argv) != 2:
        aesl_file = None
    else:
        aesl_file = os.getcwd() + '/' + sys.argv[1]
    with Thymio(aesl_file,options.system) as thymio:
        # Fonction callback pour traiter les évènements provenant du Thymio 
        # Elle sera appelée à chaque évènement du Thymio
        def dispatch(evt_id, evt_name, evt_args):
            # BUG: Ce callback n'est jamais appelé, 
            # normalement il est censé capturer les évènements emis par le robot.
            print(evt_name)


        # Cette méthode sera exécutée toutes les 100ms
        gobject.timeout_add(100, main, thymio)
        thymio.loop(dispatch)
