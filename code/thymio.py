# coding: utf8

# Dbus will allow python to communicate with the thymio through asebamedulla
import dbus
import dbus.mainloop.glib
# Gojbect is used to run and handle the loop
from gi.repository import GObject as gobject
from optparse import OptionParser
# Sys will be used to receive key presses
import sys
import os
import time
import tempfile

class ThymioException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class Thymio():
    def __init__(self, aesl_file = None, system_bus = False, node_name = "thymio-II"): 
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.node = node_name
        if system_bus:
            self.bus = dbus.SystemBus()
        else:
            self.bus = dbus.SessionBus()

        try:
            self.network = dbus.Interface(
                    self.bus.get_object('ch.epfl.mobots.Aseba', '/'), 
                    dbus_interface='ch.epfl.mobots.AsebaNetwork')
        except dbus.exceptions.DBusException:
            raise ThymioException("Can not connect to Aseba DBus services! "
                                    "Is asebamedulla running?")
            
        if aesl_file is not None:
            print(aesl_file + " loading into Thymio")
            self.network.LoadScripts(aesl_file)
            print("done")

        # Create an event filter and catch events
        self.eventfilter = self.network.CreateEventFilter()

        self.events = dbus.Interface(
            self.bus.get_object('ch.epfl.mobots.Aseba', self.eventfilter),
            dbus_interface='ch.epfl.mobots.EventFilter')
        
        # Listen events (Apparemment ça ne fonctionne pas)
        # for evt in SelectedEvents:
        #     self.events.ListenEventName('fwd.'+evt)

        # Settle an event loop
        self.gloop = gobject.MainLoop()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.events.Free()

    def dbus_reply(self):
        pass

    def dbus_error(self, e):
        # there was an error on D-Bus, stop loop
        self.stop()
        raise Exception('dbus error:' + str(e))

    def loop(self, callback):
        ''' Bind the callback and enter a loop
        '''
        # BUG:callback n'a jamais été appelée
        self.events.connect_to_signal('Event', callback)
        self.gloop.run()
    
    def stop(self):
        self.gloop.quit()

    def set(self, var, vals):
        self.network.SetVariable(self.node, var, [dbus.Int16(x) for x in vals])
        return self

    def get(self, var):
        dbus_array = self.network.GetVariable(self.node, var)
        size = len(dbus_array)
        if (size == 1):
            return int(dbus_array[0])
        else:
            return [int(dbus_array[x]) for x in range(0,size)]

    # Déclencher un évènement pre-programmé dans le robot
    def send_event(self, name, arguments = []):
        self.network.SendEventName(name, arguments,
                                    reply_handler=self.dbus_reply,
                                    error_handler=self.dbus_error)
        print("Event " + name + "has been sent.")
        return self


# Une petite démo
# def exemple(thymio):
#     groundReflected = thymio.get("prox.ground.reflected")
#     intensity = groundReflected[1] - 525
#     if abs(intensity) < 170:
#         thymio.set("motor.left.target", [115 + intensity//8])
#         thymio.set("motor.right.target", [115 + intensity//8])
#     else:
#         thymio.set("motor.left.target", [intensity//4])
#         thymio.set("motor.right.target", [-intensity//4])
#     # Pour déclencher un évènement. (Il faut que cette méthode soit en écoute par le robot)
#     # thymio.send_event('fwd.temperature', [0])
#     return True

# if __name__ == '__main__':
#     # check command-line arguments
#     if len(sys.argv) != 2:
#         aesl_file = None
#     else:
#         aesl_file = os.getcwd()+'/'+sys.argv[1] 
#     with Thymio(aesl_file) as thymio:
#         # Fonction callback pour traiter les évènements provenant du Thymio 
#         # Elle sera appelée à chaque évènement du Thymio
#         def dispatch(evt_id, evt_name, evt_args):
#             # BUG: Ce callback n'est jamais appelé, 
#             # normalement il est censé capturr les évènement emit par le robot.
#             print(evt_name)
#         # Cette méthode sera exécutée toutes les 100ms
#         gobject.timeout_add (100, main, thymio)
#         thymio.loop(dispatch)